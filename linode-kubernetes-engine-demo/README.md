# Helm Demo: 
## Install a Stateful Application on K8s using HELM



### Created K8s cluster on Linode Kubernetes Engine

- Created a "Kluster" in Linode Cloud Platform with 2 nodes and 1 control plane.

- Download the configfile for Kubeconfig `Kluster-kubeconfig.yaml`

- Configure it as an environment variable `export KUBECONFIG=Kluster-kubeconfig.yaml`

- Now with `kubectl get node` you should get your nodes. 

### Deployed replicated MongoDB (StatefulSet using Helm Chart) and


### Conﬁgured Data Persistence with Linode Block Storage


### Deployed MongoExpress (Deployment and Service)


### Deployed NGINX Ingress Controller as Loadbalancer (using Helm Chart)


### Conﬁgured Ingress rule